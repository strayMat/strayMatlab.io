# 👨‍🏫 Teaching

## 2025

- I am giving half of the `Machine Learning for econometrics` course at the [ENSAE](https://www.ensae.fr/) in 2025. [Course material is avaible on github](https://github.com/strayMat/causal-ml-course) (heavily inspired by the excellent [sklearn mooc](https://inria.github.io/scikit-learn-mooc/)). I teach about statistical learning, causal inference and panel data. 

## 2024-2025

For the second year, I am giving one session during the course on health data at Université Paris Cité ([Master Santé publique - Parcours : Méthodes et Outils pour les Données des Entrepôts en Santé](https://odf.u-paris.fr/fr/offre-de-formation/master-XB/sciences-technologies-sante-STS/sante-publique-K2NDGZO3/master-sante-publique-parcours-methodes-et-outils-pour-les-donnees-des-entrepots-en-sante-ancien-libelle-donnees-massives-en-sante-K168SJQL.html)). The course targets public health medical students and aims at presenting the current state of health data warehouse in France, as well a recent study on quality of care using this data.

- [Slides](_static/files/presentations/2025-03-12_Cours_données_de_santé_edsh.pdf)