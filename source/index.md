```{toctree}
:maxdepth: 1
:hidden:

👨‍🔬 Research <research_interests/index.rst>
👨‍🏫 Teaching <teaching.md>
👨‍💻 Other projects <projects/index.md>
📰 Blog <blog/index.md>
```

# About

- Project lead for Health Data Warehouse projects and Large Language Models in the data team at the [Haute Autorité de Santé](https://www.has-sante.fr/).

# News 

- 🎉My paper [Step-by-step causal analysis of EHRs to ground decision-making](https://journals.plos.org/digitalhealth/article?id=10.1371/journal.pdig.0000721) has been published in PLOS digital health ! With the other authors, we explore the main sources of variability when running a sound causal inference analysis on Electronic Health records. I also tried to make it a good tutorial for data analysts interested in applying causal inference methods with observational data.

- 👨‍🏫 I am giving half of the `Machine Learning for econometrics` course at the [ENSAE](https://www.ensae.fr/) in 2025. [Course material is avaible on github](https://github.com/strayMat/causal-ml-course) (heavily inspired by the excellent [sklearn mooc](https://inria.github.io/scikit-learn-mooc/)). I teach about statistical learning, causal inference and panel data. 

<p align="center">
<img src="_static/files/images/portrait_vercors.png" alt="drawing" width="180"/>
</p>
<p style="text-align: center;">
    <a href="_static/files/cv_fr.pdf">CV</a> - <a href="matt dot dout[@]gmail dot com">Contact</a>
</p>

# Past occupations

- 2020 - 2024: Project manager, statistician at the Haute Autorité de Santé (HAS) data mission. Referent on health data warehouse projects, methodological referent in statistics. Assists artificial intelligence projects with literature reviews. 

- 2020 - 2023: PhD student in the  [SODA team](https://team.inria.fr/soda/) at Inria under the supervision of Gaël Varoquaux (Inria) and Dr. Claire Morgand (ARS-IDF). In my research projects, I have applied methods combining causal inference and statistical learning to data from health data warehouses and medico-administrative databases.
    - 📓 **Thesis manuscript** : [Representations and inference from time-varying routine care data](https://theses.hal.science/tel-04343116)
    - 👨‍🏫 **Presentation for the defense** : [Slides](_static/files/presentations/2023-11-20_soutenance.pdf)


- 2020, Covid19, 1st epidemic wave: Ministry of Health crisis center, various data pipelines and strategic dashboards.

- 2018 - 2020: Machine learning engineer at the statistical department of the Ministry of Health, [DREES](https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/), working mainly on the National Health Data System, [SNDS](https://documentation-snds.health-data-hub.fr/).

- 2018 : At AP-HP, I set up the first [pseudonymisation system for medical records](projects/neuroNer.md) based on neural networks.

- 2017 : Master student at MVA, ENS Paris-Saclay in Artificial Intelligence and at ENSAE in statistical learning.

- 2014-2017 : Ecole Polytechnique, Applied Mathematics

# Publications

[Google scholar](https://scholar.google.com/citations?user=lXOz9tkAAAAJ&hl=fr&oi=ao)