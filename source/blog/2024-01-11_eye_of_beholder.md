---
blogpost: true
date: 2024-01-11
category: Papers
tags: [Statistics, Epidemiology, Epistemology]
language: Français
---


# The eye of the beholder

How do public health researchers interpret regression coefficients ? 

Paper: [Collyer, T. A. (2024). The eye of the beholder: how do public health researchers interpret regression coefficients? A qualitative study. BMC public health, 24(1), 10.](https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-023-17541-3)

The author anchors the uncertainty about regression coefficients into the [Duhem-Quine paradox](https://en.wikipedia.org/wiki/Duhem%E2%80%93Quine_thesis): Results from a scientific experiment cannot be tested in isolation and require background assumptions. Unexpected effect size could either be to a discovery or to poor experimental conduct (design, confounding, other bias...).

Mechanical processes entry into scientific thinking, sometimes replacing human judgment. An interesting parallel is the one between the mechanical objectivity of automated imaging technology such as x-ray, introduced in the late 19th century and the mechanical objectivity of automated statistical analysis such as the Null Hypothesis Significance Testing in the mid 20th century. In both case, however, calibration by humans is required.

The semi-structured interviews was conducted with 45 population health researchers from various background and countries (all western economies though). Strikingly, they generally do not have a preconception of where regression stands between knowledge discovery and knowledge construction.  

The interesting view of Latour et Woolgar (Laboratory Life: the construction of scientific facts, 1986) is that you are able to distinguish between discovery and construction only a posteriori after a statement has stabilized as a fact.

My answer to this question would have been : First, the coefficient could be attached to an objective reality (a concept on which most would agree), mostly in the cases of underlying human interventions. Otherwise, I am standing with the ML community, and I would prevent from attributing a causal interpretation to the inner part of a model. Second, if we are in the case of an reasonably well identifiable coefficient (supposing appropriate design and confoundings), the coefficient is on one part a discovery, since it informs on how effective is an intervention, but on the other part the intervention and data collection themselves are a construction. Both are consolidating heterogenous phenomena into clearly separable categories. These constructions are making choices among distinct realities, and thus impoverish them. This line of thought is quite well expressed in the work of Alain Desrosières, "La politique des grands nombres, 1993". 