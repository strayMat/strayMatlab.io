---
blogpost: true
category: Veille
tags: [Machine Learning, NLP]
language: English
---

# LLMs architectures

I recently wondered what was the architectures of recent LLMs such as Llama2, mistral or ChatGpt ? Of course, these are based on transformers. But are they only decoders ? How many layers are they? How many heads. I don't want to retain all the details, but I would like to have some order of magnitude in mind.


## [Llama2 paper inspection](https://arxiv.org/pdf/2307.09288)

This [blog post](https://cameronrwolfe.substack.com/p/llama-2-from-the-ground-up) better explain the full architecture: Multiple decoder-only transformer blocks. The main modifications from the original transformer paper are : 

- Root Mean Square Layer normalization applied before the attention and MLP blocks. This is a simplified version of the Layer normalization, which yields more stability and generalization.

- [SwiGLU activation function](https://arxiv.org/pdf/2002.05202v1.pdf): more computationally expensive but more performant (not clear why?).

- Rope embedding adopting a trade-off between absolute and relative positional embeddings. 

The majority of these improvements were made to improve the inference efficiency
of the model.


## [Mistral paper inspection](https://arxiv.org/pdf/2401.04088)


## [Olmo paper inspection](https://allenai.org/olmo/olmo-paper.pdf)

Contrary to LLama2, they describe very precisely their architecture. They adopt the exact same choices as Llama2 : from 16 to 80 attention layers with hidden size from . 

They provide a clear architecture comparison which best summarize the questions that I was asking myself : 

![](/_static/files/images/open_llm_architecture_comparison.png)
