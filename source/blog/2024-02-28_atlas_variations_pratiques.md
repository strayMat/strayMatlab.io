---
blogpost: true
date: 2024-02-28
category: Papers
tags: [Statistics, Healthcare, Economics]
language: Français
---

# Atlas des variations de pratiques médicales

Quelles sont les variations de pratiques médicales en France ? Quels sont les facteurs qui les expliquent ?

Papers :  [Atlas des variations de pratiques médicales, Zeynep Or, Julie Cartailler, Morgane Le Bail, Irdes, 2024](https://www.irdes.fr/recherche/ouvrages/009-atlas-des-variations-de-pratiques-medicales-recours-a-onze-interventions-chirurgicales.pdf)


## Motivation

Les pratiques médicales varient d'un territoire à l'autre. Ces variations peuvent être le signe de demandes de soin hétérogènes, d'une inégalité d'accès aux soins, ou de qualité des soins différentes.

Depuis 2015, l'assurance maladie a développé un ciblage des établissements de santé atypiques basés sur les indicateurs d'alerte issus des référentiels de la HAS portant sur le PMSI. Ce ciblage a pour but de déceler des établissement s'écartant fortement des pratiques médicales du département sans que cela soit expliqué par des indicateurs démographiques ou épidémiologiques.

Ce second atlas des variations de pratiques médicales a pour objectif de mesurer et suivre dans le temps les taux de recours de onze interventions. Il vise également à élargir le cadre du suivi et de l'évaluation des pratiques en proposant de nouvelles indications et de nouveaux indicateurs de processus de soin : Récupération améliorée après chirurgie et taux de réhospitalisation à 30 ou 90 jours.

Les interventions chirurgicales étudiées ont été sélectionnées pour leur volume, une tendance à la hausse, et de fortes variations régionales. Un autre critère est leur forte élasticité vis-à-vis de l'offre de soin identifiée dans la littérature internationale.

## Résumés des onze interventions

![](/_static/files/images/atlas_pratiques_summary.png)

Les principales mesures données sont le nombre d'actes, le taux de recours standardisé par l'âge et le sexe pour 100 000 habitant ($tx$), la variation interdépartementale définie comme le rapport entre la déviation standard interdéparmentale du taux de recours et le taux de recours moyen.

### Prothèse de hanche après fracture

En 2019, 36 000 actes. Faible variation interdépartementale à 6\%. Légère diminution du taux standardisée. Faible recours à la Raac avec une grande variation interdépartementale (25\% n'en ont codé aucune). Une comparaison internationale (Papanicolas et al., 2021) conclue à des taux de réadmission à 30 ou 90 jours élevés en France (12\% et 22\% en 2019).


### Prothèse du genou

En 2019, 115 000 actes et $tx=171$. Forte variation interdépartementale à 17\%. Augmentation de 10 % du taux de recours standardisé depuis 2014.

Recours à la Raac de 31\% avec une grande variation interdépartementale (de 60\% à 4\%).

### Chirurgie du syndrome du canal carpien

En 2019, 145 000 actes et $tx=216$. Forte variation interdépartementale à 27\%. Légère baisse du taux de recours standardisé et stabilité de la variabilité interdépartementale. Presque exclusivement en ambulatoire (96\%).

Maladie professionnelle très courante. 

### Chirurgie de la cataracte

En 2019, 920 000 actes et $tx=1 374$. Forte variation interdépartementale à 10\%. Forte hausse du taux standardisé (+10\%). Les forts recours en France, comparativement au reste de l'Europe a conduit à une des recommandations HAS en 2019. Presque exclusivement en ambulatoire (95\%). Peu de variation du taux de réadmissions sur la période (~3\%).

Concerne 1/5 après 65 ans et 2/3 après 80 ans. 

### Amygdalectomie 

En 2019, 58 388 séjours soit une diminution de 12\% par rapport à 2014 (tx=9\% en taux standardisé). Variations départementales importante à 28\%. La principale complication est l'hémorragie (dans les 8h ou dans les 15 jours). L'ambulatoire est recommandé sans risque particulier et s'il est possible de surveiller le patient au domicile. Le taux de réadmission à 30 jour est stable à 6\% mais avec une forte variation départementale (écart type 25).

### Pose de stent coronaire sans infarctus du myocarde

Sans infarctus du myocarde, cet acte n'est recommandé qu'en cas de syndromes persistants sous traitement médical. 

En 2019, 108 945 séjours, en augmentation depuis 2014, passant de 146 à 163 séjours en taux standardisé. La variation départementale est forte à 36\%. 

Le taux standardisé de réadmission à 30 jours est de 16\% en 2019 en légère augmentation depuis les 15\% de 2014.

### Chirurgie bariatrique ou de l'obésité

Le taux standardisé a diminué sur la période (de 88 à 83) pour un total de 55 787 interventions en 2019. La variation interdépartementale est forte mais a fortement diminué de 38\% en 2014 à 28\%. Ces améliorations peuvent être expliquées par plusieurs politiques publiques : IQSS HAS en 2017, Plans d'actions régionaux puis Caqes des ARS, Mise sous accord préalable de la CNAM. Malgré une étude récente sur les bénéfices de la Raac, le taux de séjour y ayant recourt est faible (24\%) avec une forte variation interdépartementale (écart type de 23). Les taux de réhospitalisation à 30 jours sont en légère augmentation (de 5.5 à 6.1\%).

### Cholécystectomie ou ablation de la vésicule biliaire

En légère diminution sur la période, le taux de recours est passé de 176 à 171 avec un total de 114 741 interventions en 2019. La variation interdépartementale est stable à 15\%. 
L'abord par coelioscopie peut se faire en ambulatoire et donne lieu à une forte progression de la prise en charge ambulatoire de 20 à 40\% en 2019. Le taux de réadmission à 30 jours est de 6\% en 2019.

### Césarienne

Le nombre de Césarienne a diminué à 141 644 actes en 2019 mais principalement dû à une diminution des naissances : le taux pour 1000 naissances est stable à 190 / 1 000 naissances. La variation interdépartementale est de 21 SD. Cette variabilité est connue depuis longtemps, et a donné lieu à plusieurs programmes : communications ou contrat de bonne pratique avec les établissements de la part des ARS. Ces programmes semblent fonctionner dans les départements y ayant eu recours. Des dispositifs spécifiques régionaux sont chargés de diffuser les recommandations de la HAS. 

Les taux de réadmission à 30 jours sont stables à 1\% mais la variation départementale a beaucoup augmenté avec certains départements au delà de 3\%.


### Hystérectomie (ablation de l'utérus)

Le nombre d'intervention a diminué à 55 496 en 2019 avec un taux de recours passant de 334 à 297 pour 100k femmes de plus de 40 ans. La variation interdépartementale est de 15\%. La Raac est présent dans seulement 10\% des séjours avec une forte variation interdépartementale (87\%). La réadmission à 30 jours est de 7\%, stable sur la période, principalement pour saignement.

### Chirurgie de la tumeur bénigne de la prostate

L'Hypertrophie bénigne de la prostate (HBP) augmente chez tous les hommes âgés (50\% des hommes de 60 à 70 ans, puis plus de 82\% après 71 ans) . Elle peut entraîner des troubles urinaires par compression de l'urètre si elle est avancée. Si les troubles deviennent trop important, une chirurgie est indiquée.

Le nombre de chirurgie a augmenté à 61 428 séjours en 2019, mais le taux standardisé a diminué de 387 à 376 depuis 2014. Les variations interdépartementales sont stables à 16\%. Le taux de séjour en ambulatoire est faible à 6 \% en moyenne en 2019. Le taux de réhospitalisation à 30 jours est de 13\%  

## Ouverture

Ce pannel d'interventions semble mettre en lumière certains département étant systématiquement en bas ou en haut de la distribution départementale des taux de recours. Est-ce que ce caractère systématique sur plusieurs permet de relever une sous-qualité des soins ou un défaut d'offre ?

Sans qu'un seuil soit donné, une variation interdépartementale au delà de 20 \% est considérée comme importante dans le texte de l'atlas. Est-ce que ce type de seuil est pertinent au niveau établissement ? Est-ce que ce seuil est une référence quelque soit la pratique pour considérer que la variabilité interdépartementale est anormale ? 