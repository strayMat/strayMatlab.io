---
blogpost: true
date: 2024-04-29
category: Papers
tags: [Statistics, Epidemiology, Causality]
language: Français
---


# On the usage of bootstrap for causal inference estimators 

## Interrogation on the variance of the inverse probability-of-treatment weighting estimator

What is the variance of the IPTW estimator when taking into account the estimation step ?

Paper: [Kostouraki et al. (2024). On variance estimation of the inverse probability-of-treatment weighting estimator: A tutorial for different types of propensity score weights. Statistics in Medicine](https://onlinelibrary.wiley.com/doi/10.1002/sim.10078)

## Motivation

Taking into account the variance of the IPTW estimator is not trivial since some variance is induced by the estimation of the propensity score (treatment probabilities), $e(x) = P(W=1|X)$. Most theoretical results takes the propensity scores as given, then compute the variance of the oracle IPW estimator with oracle propensity score.For example, [Wager, 2024, Th. 2.2](https://web.stanford.edu/~swager/causal_inf_book.pdf) derive the oracle IPW variance as: 

$
Var(\hat{\tau}^{\star}_{IPW}) = Var[\tau(X_i)] + \mathbb{E} \left[ \frac{\left( \mu_{(0)}(X_i) + (1 - e(X_i)) \tau(X_i) \right)^2}{e(X_i)(1 - e(X_i))} \right] + \mathbb{E} \left[ \frac{\sigma^2_{(1)}(X_i)}{e(X_i)} + \frac{\sigma^2_{(0)}(X_i)}{1 - e(X_i)} \right].
$

The sole source of variance assumed is due to data sampling through the variance. How to take into account the variance of the propensity score estimation ?

😍 Kostouraki et al. (2024). state *Non-parametric bootstrap can be used to obtain valid SEs, but the bootstrap may be computationally intensive for large databases.* So, the authors are concerned with analytical approaches in which I am a bit less interested in due to the induced complexity. 

Note for later: I still wonder if bootstrap is valid for g-estimation ? The [marginal effect blog](https://marginaleffects.com/vignettes/gcomputation.html) let me think that it is the case. It compares the delta method to an example of Pearl using bootstrap and conclude that the results are close but not identical. However, there is no reference for a proof. A good mathematical exploration of this question in the randomized case is given by [Imbens and Menzel. 2018. Causal boostrap](https://www.nber.org/system/files/working_papers/w24833/w24833.pdf). It concludes that the bootstrap is conservative. 