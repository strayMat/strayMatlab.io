---
blogpost: true
date: 2024-01-05
category: Papers
tags: [Statistics, Healthcare, Causality]
language: Français
---

# Méthodes quantitatives pour évaluer l'impact d'interventions

Comment évaluer l'effet d'une intervention? L'idéal reste l'essai contrôlé
randomisé. Pour une intervention en santé, c'est l'essai par grappe qui prévaut
car l'intervention porte sur un collectif (ex: un groupe de médecin) alors que
la mesure de résultat *outcome* porte sur les patients. Des schémas
quasi-expérimentaux sont également envisageables lorsque la randomisation est
compliquée.

Publication: [Méthodes quantitatives pour évaluer les interventions visant à améliorer les pratiques, guide méthodologique HAS, 2007](https://www.has-sante.fr/upload/docs/application/pdf/eval_interventions_ameliorer_pratiques_guide.pdf)


## Essais par grappe

L'essai par grappe répond à la non-indépendance des patients soignés par un même
groupe de profesionnels de santé (médecin, hôpitaux...). On randomise donc les
médecins au lieu de randomiser les patients comme dans un essai classique.
L'ensemble des patients soignés par un même médecin constituent une grappe.

Les principales limites du schéma par grappe sont:
- Le nombre de grappes est souvent faible (ex: 10 médecins), augmentant la
  probibilité de non comparabilité des groupes contrôles et traitements.
- La difficultés à rendre ce schéma aveugle: les médecins peuvent ne pas
  appliquer l'intervention qui leur est alloué. Plus simplement, les médecins du
  groupe traitement peuvent être plus motivés que les médecins du groupe
  contrôle, engendrant une surestimation de l'effet de l'intervention: *effet
  Hawthorne*.
- La nécessité de prendre en compte l'effet grappe (cluster effect): si les
  malades se ressemblent beaucoup au sein de chaque grappe, il faudra plus de
  grappes et donc plus de patients au total pour obtenir la même puissance
  statistique qu'un essai classique.

Des variantes d'essais en cluster existent: essais factoriels (combiner
plusieurs interventions sans interactions peut augmenter la puissance
statistique), essais equilibrés incomplets (afin d'éviter l'effet de Hawthorne).

## Etudes quasi-expérimentales

Elles consistent à vérifier si une intervention a modifié une tendance. L'étude
la plus simple consiste à une étude de séries chronologiques consistant à
comparer la rupture de tendance avant, pendant et après l'intervention. En cas
de non rémanence de l'effet de l'intervention, on peut procéder à des études
avant après (intutition similaire à celle derrière les self-controlled studies):
ce cas d'usage est intéressant lorsqu'on étudie le déploiement d'un nouvel outil
informatique auquel on peut couper l'accès.  

## Particularités générales des études d'intervention

- standardisation de l'intervention (consistency en inférence causale)

- les critères de jugement doivent porter sur une mesure réelle (objective?) des
  pratiques et non une modification des connaissances ou des jugements.

- Le critère de jugement doit être l'adhérence à une pratique et non le résultat
  de cette pratique (ou non pratique) sur les patients. En effet, les résultats
  doivent être mesuré à l'aide d'un essai clinique constituant un meilleur
  niveau de preuve. Les arguments présentés sont surtout basés sur le nombre de
  patients plus grand que requiert l'évaluation sur résultat. 

  ## Discussion

  ### Lien avec le cadre [quasi-expérimental](https://en.wikipedia.org/wiki/Quasi-experiment)

  Le cadre quasi-expérimental est plus large que les séries chronologiques (interrupted time series). On y trouve également les méthodes suivantes : 
  
  - Nécessitant un groupe contrôle : le [diff-in-diff](https://en.wikipedia.org/wiki/Difference_in_differences) fait l'hypothèse d'une tendance parallèle entre les deux groupes. Cette forte hypothèse est parfois plus crédible en incluant des variables de contrôles pré-traitement dans la modélisation.  

  - Sans groupe contrôle : la [regression discontinue](https://en.wikipedia.org/wiki/Regression_discontinuity_design) (expliquée en détail [dans le cours de Wager](https://www.jelenabradic.net/uploads/5/8/2/7/58270017/stats361.pdf#chapter.5)) suppose une discontinuité nette entre le groupe traité et non traité selon une variable continue (par exemple, au dessus ou en dessous d'un certain score ou d'un temps précis). L'effet est mesuré en comparant les deux groupes autour de la discontinuité.

  En lisant [ce tutoriel sur les interrupted time series (Lopez Bernal et aL., 2017)](https://academic.oup.com/ije/article/46/1/348/2622842), j'ai l'impression que ce design réduit le problème causal à une rupture de tendance pour une fonction du critère de jugement (outcome) en fonction du temps. Il fait donc l'hypothèse que les confondants ne varient pas ou peu lors de la période d'étude: une hypothèse raisonnable sur une courte période historique, mais qui ne tient pas compte de confondants temporels ou des chocs concomitants à l'intervention (par exemple un congrès de médecine). 

  ### Potentiel d'application en santé

  Mener des études par série chronologique avec des actions répétées à
  destination de professionnels ciblés permettrait d'avoir une idée assez
  précise de l'effet d'une intervention sur le changement des pratiques. Dans le
  cas idéal, on pourrait mener des études randomisées par grappe, mais une
  simple période de communication avec mesure des pratiques avant/après
  permettrait d'avoir des résultats convaincants en comparant les tendances
  pré-intervention/pendant intervention et post-intervention. On pourrait se limiter
  dans un premier temps aux recommandations récentes avec un certain nombre de
  téléchargement mesurables dans le SNDS.

  ## Références supplémentaires

- [Ewusie et al., 2020, Methods, Applications and Challenges in the Analysis of Interrupted Time Series Data: A Scoping Review, Journal of Multidisciplinary Healthcare](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7231782/) : Une revue sur les ITS et leur dissémination.
- [Brodersen, K. H., Gallusser, F., Koehler, J., Remy, N., & Scott, S. L. (2015). Inferring causal impact using Bayesian structural time-series models.](https://www.jstor.org/stable/24522418) : Le papier derrière la librairie open source [Causal Impact](https://google.github.io/CausalImpact/CausalImpact.html) de google. Une idée très intéressante est d'utiliser des time series de confondants. Il est assez facile de trouver des time series corrélées à la série de l'outcome. Pour certaines d'entre elles, il est facile de se convaincre qu'elles ne sont pas des colliders directs (cf. [principle of confounder selection, VanderWeele](https://link.springer.com/article/10.1007/s10654-019-00494-6) pour une explication simple sur les différents types de variables causales) et de les utiliser comme des prédicteurs.