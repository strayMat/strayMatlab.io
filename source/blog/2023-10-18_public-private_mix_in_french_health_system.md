---
blogpost: true
date: 2023-10-18
category: Papers
tags: [Economics, Healthcare]
language: English
---

# Is Public-Private Mix in French Health System Sustainable?

Three major values --solidarity, liberalism, and pluralism-- underpins the French Health System. Despite low out-of-pocket payments,its joint reimbursement of the same health service by both private and public insurance raises equity, efficiency, and sustainability challenges.

Paper: [Is Public-Private Mix in French Health System Sustainable?, PIERRE A. ET OR Z., 2023, Document de travail Irdes](https://www.irdes.fr/english/working-papers/091-is-the-public-private-mix-in-french-health-system-sustainable.pdf) 


Three major values --solidarity, liberalism, and pluralism-- underpins the French Health System. 

Differ from other private-public mix by joint reimbursement of the same health
service. 

The basis of solidarity: access to care depending on need, not income
(horizontal equity) and solidarity between high- and low-income classes
(vertical equity). 

## Insurance 

The comprehensive health service package covered by the Statutory Health
Insurance (SHI) covers about 80% of the total cost of health care in 2021: 93%
for hospital care, 8% for ambulatory treatments, and 44% for drugs and medical
goods. These quantity are (slowly) increasing.

Private complementary insurances are crucial to cover 13% on average of health
care expenditure: medical goods (38%), outpatient cares (22%), drugs (11%).

These companies are heavy regulated: loi Evin assuring lifetime guarantee for
anyone insured. Tax incitation for responsible contracts that do not adjust on
health status. 

Concerns for equity contravening to the horizontal equity principle are: private
insurance premiums contract conditioned on age (strong proxy of risk),higher
part of the income spent on private health  insurance for lower income classes,
unequal access to some practitioners because of the insurance status (although
illegal).

Efficiency concerns come from the multiplicity of payers for the same basket of
care, inflationary incitations from generous CHI coverage, high administrative
costs (14 billions, 50% linked to private insurance). 

## Health providers

With 3.2 physicians, 10.5 nurses per 1,000 population in 2019, France is close
to OECD average. However high heterogeneity. Outpatient care is given by 47% of
doctors and 65% of GP that are self-employed in 2016. Secteur 1 respecting
fee-for-services negotiated annually and secteur 2 with about 52% higher fees,
but practitioners must purchase their own pension and insurance coverage.
Secteur 2 is regulated since 1990 because more physicians than
predicted entered it in 1980.

Highest hospitalization rate in the OECD despite decreased number of beds (50%
of spending). 45% of public hospitals, 25% of private for-profit: not positioned
on the same care segments (eg. market share of up to 65% for knee replacement,
80% for cataract or endoscopies).

The *Tarification à l'activité* introduced in 2005 has been initially welcomed
by all actors. However, it created extra-fees mostly in for-profit
organizations. Successful regulation by the public insurance managed to contain
it to about 45% in 2016 compared to 80% in 2005. But this payment model had bad
consequences on care quality and allocation efficiency.

Concerns about the sustainability of freedom of installation  are increasing in
a context of unequal repartition of the population. Some specialties display
increasing waiting time (with high variance). Tentative to improve care pathways
through mandatory GP remained unsuccessful because no incentive exist for
collaboration and care coordination. 

Concerns about extra-fees in ambulatory cares are persistent, especially for
specialist and for the sickest patients. In 2007, the General Inspectorate of
Social Affairs revealed that the average amount of extra-billing between 1995
and 2004 increased 3 times faster than average incomes in France. By allowing
some practitioner to modulate their level of activity or productivity, it
accentuates geographical disparities.

France had historically a regulation focused on health care costs. However, it
fails to control the growth of total care expenditures because of volume
compensation by the providers. The system encourages more hospital utilization,
medical tests and medications with high risk of duplication of services and
inefficient care process.

The ONDAM mechanism focused on aggregated targets with lower prices if volume
augments too quickly, leading to the prices becoming (progressively) unrelated
to hospitals' costs. 

Regulating volume rather than quality has been done through price regulation for
fast growing activities with high variation between regions and potentially
harmful consequences for patients. There has been a backward trend in collecting
quality data such as 30-day readmission rates, mortality, adverse events or
patient experiences in primary care.

Successful innovation incentive collaborations such as article 51
experimentation,  bundled payments GHT and CPTS. But France needs to refine and
diffuse indicators for benchmarking the quality of care across settings
(readmissions, complications rates, inappropriate prescriptions, etc.). There is
also a need for more public information on prices. 

The authors also are in favor of dramatically reducing the market share of the
private insurance. 