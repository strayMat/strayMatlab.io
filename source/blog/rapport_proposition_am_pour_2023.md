Chaque année, l'assurance maladie publie un rapport très complet analysant le système de santé et résumant ses propositions pour l'année à venir.

On peut trouver ces rapports dans [la partie publication du site de l'Assurance Maladie](https://assurance-maladie.ameli.fr/etudes-et-donnees/etudes-publications/assurance-maladie/rapport-propositions-assurance-maladie-charges-produits). 

Regardons par exemple [le rapport annuel de propositions pour 2023](https://assurance-maladie.ameli.fr/etudes-et-donnees/2022-rapport-propositions-pour-2023-charges-produits).

Enfin, regardons plutôt [la synthèse du rapport annuel des propotions pour 2023](https://assurance-maladie.ameli.fr/sites/default/files/2022-07_synthese-rapport-propositions-pour-2023_assurance-maladie_1.pdf) ! 


# Introduction 

Le rapport souligne l'importance que doit prendre la santé publique et la prévention dans les politiques de santé. 

Il est nécessaire de pouvoir répondre aux urgences de très court terme mais également de mener des réformes en profondeurs du système pour assurer sa pérennité.

La branche maladie est confrontée à un *déficit inédit et durable*. Il est donc nécesaire d'analyser finement les postes de dépense et de les mettre en vis-à-vis des de leur destination médicale et de leurs résultats.


# Quelques chiffres sur la cartographie des pathologies

La [cartographie des pathologies et des dépenses](https://assurance-maladie.ameli.fr/etudes-et-donnees/par-theme/pathologies/cartographie-assurance-maladie) est une base aggrégée du SNDS, par type de pathologie .
Cette base est très utile afin de mieux comprendre les principaux postes de dépenses de la CNAM. Depuis 2021, l'application web [data-pathologie)(https://data.ameli.fr/pages/data-pathologies/) sur le site amelie présente l'historique de ces données sous forme de graphiques.  

Voici quelques chiffres clés tirées de cette base de données en 2020 :

- 36% de la population prise en charge pour pathologie et/ou traitement chronique.
- 168 milliards d'euros remboursés (+10% par rapport à 2015) pour 66,3 millions de personnes prises en charge.
Des augmentations 
- Parmi les pathologies ayant les évloutions les plus fortes depuis 2019 on trouve :
  - les cancers actifs en progression de +4,9 %
  - les traitements psychotropes en progression de +6,2 %
  - les diabètes en progression de + 4,5 %
  - les hospitalisations ponctuelles en diminution de - 10,6 %

Le graphique présentant les effectifs en fonction des dépenses moyennes par pathologie permet d'apprécier l'importance relative des différentes pathologies dans le budget de la CNAM : 

![top_pathos_effectifs_depenses_moyennes_2020](/_static/files/images/top_pathos_effectifs_depenses_moyennes_2020.png)

# Quelques chiffres concernant l'offre de soin

Les effectifs des professionnels de santé libéraux sont en augmentation avec une progression ed 40 % depuis l'année 2000. Les professions qui ont le plus progressés sont les infirmières (99 099 en 2021, +98 %), les masseurs-kinésithérapeutes (73 457 en 2021, +88 %), et les sages-femmes (7223 en 2021, +321 %). En revanche le nombre de médecin libéraux a diminué (112 742 en 2021, -3,4 %).

> Je me demande quelle est l'évolution de ces chiffres sur l'ensemble de la tranche de 20 ans.

## Poste de prescription des médecins généralistes en 2021

## Prévention

# Approche par pathologie

## Insuffisance cardiaque

## Diabète

## Impact Covid 

## Maternité et petite enfance

# Analyse sectorielle 

# Organisation et accès aux soins

# Efficience et pertinence des soins

