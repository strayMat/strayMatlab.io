---
blogpost: true
date: 2023-04-05
category: Papers
tags: [NLP, Healthcare]
language: Français
---

# Un Large Language Model français pour le domaine médical

Une équipe de Nantes a entraîné [Dr Bert](https://drbert.univ-avignon.fr/) un modèle de langage RoBERTa en Français sur des textes cliniques en tirant profit du super calculateur Jean Zay. Il y a une version privée et une version publique du modèle selon s'il a été entraîné sur des données du corpus NBDW privé du CHU de Nantes ou les données Nachos (corpus de données publiques).

Les tâches d'évaluation sont les suivantes: se référer aux tableaux 7 et 8 du papier pour les performances.

 - ESSAIS/CAS: Part Of Speech sur des cas cliniques
 - FrenchMedMCQA: Questions à choix multiples sur des examens de médecine
 - QUAERO: Reconnaissance d'entité nommées (NER) sur des documents biomédicaux
 - MUSCA-DET: NER et multilabel classification sur des phrases extraites et pseudonymisées de la section style de vie des notes cliniques du CHU de Nantes

 Il y aussi trois tâches privées car reposant sur des données du CHU de Nantes:
 - Technical Specialties Sorting: Classification de la spécialité du rédacteur d'un CR
 - Medical report structuration prescriptions: NER sur des éléments importants d'un CR
 - Acute Heart Failure (aHF): NER sur 46 entités importantes dans les notes cliniques concernant l'infarctus du myocarde
 - Acute Heart Failure (aHF) classification: tâche de classification de présence ou d'absence d'un infarctus du myocarde 


## Commentaires généraux

- Le modèle public DrBert NACHOS (large) est le meilleur sur les tâches publiques, et reste compétitif sur les tâches privées
- Raffiner un modèle semble en général une mauvaise idée, sauf à raffiner un modèle spécialisé anglais vers le spécialisé français
- Les tâches de classification (Musca-det T2 ou aHF classif) sont très mal effectuées par des modèles spécialisés anglais, mais les modèles français non spécialisés sont compétitifs avec les modèles français spécialisés (DrBERT/CHUBERT). Pour le NER c'est l'inverse, un modèle spécialisé anglais est compétitif avec le modèle français spécialisé. Ils ont montré dans des étude de sensibilité, que seules les couches attentionnelles importent pour ces tâches de NER (en randomisant la couche d'embedding).
- Ils ont utilisés les données de la HAS et de plusieurs autres agences pour entraîner le modèle public. 
- Ils ont réussi à transmettre des données pseudonymisés de Nantes sur Jean Zay (autorisation cnil en 2018). Afin de les sortir ils ont dû "prouver" que c'est anonyme: pour cela, ils ont découpé et mélanger les phrases.

## Commentaires techniques

- Les modèles ont été entraînés à budget de calcul constant: 128 GPU V100 à 32GB pendant 20 heures.
- Les paramètres ont été conservés depuis le papier RoBERTa: warm up linéaire du learning rate.
- Checkpoint régulier jusqu'à épuiser le budget de calcul.