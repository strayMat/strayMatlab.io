---
blogpost: true
date: 2024-01-28
category: Papers
tags: [Economics, Healthcare]
language: English
---

# The end of an era? Activity-based funding based on diagnosis-related groups (DRG)

Four exit paths from a full activity-based inpatient sector explored by ten OECD countries in the last decade.


- [Milstein, R., & Schreyögg, J. (2024). The end of an era? Activity-based funding based on diagnosis-related groups: A review of payment reforms in the inpatient sector in 10 high-income countries. Health Policy, 141, 104990](https://www.sciencedirect.com/science/article/pii/S0168851023002750)
 

## What is Diagnosis-related group (DRG) funding and why do we care about it ? 

First, recall the Diagnosis-related group (DRG) funding : It maps each patient to a group based on its main inpatient diagnosis. The payment is based on the average cost of the group, adjusted by factors such as age, sex, co-mibdities, procedures reflecting resourc intensity. This system incentivizes hospitals to reduce the length of stay and to increase the number of patients treated. It was first introduced in the US in 1983. 

The negative effects of DRG are : increased volume beyond medical necessity and ignoring quality of care. Efforts to curve these effects such as pay-for-performance, increased competition, and expenditure targets had mixed and inconclusive results. 

## Results : 4 tendencies to shift away from DRG funding

![Alt text](/_static/files/images/end_of_an_era__shifting_away_drg.png)

- Searching for new combinations of payment systems with a goal to limit DRG around 50\% of hospital funding in most of the studied countries. 

- Choosing different payment programs for specific hospital types : The paper develop the example of rural hospitals that receive add-on payments. However, the few evaluation of such programs show mixed results, failing to detect a decrease in expenditure or quality improvement.

- Bridging providers via episode-based payments (ie. bundled payments): These are based on average cost for a specific episode of care, adjusting for cost-related factors (eg. age, co-morbidity, socio-economic status). In the US, a participant can either chose a one sided system where he only get the gain with respect to the average, or a two-sided system where he also bears the risk of loss but can have higher gains. In France, the gains are shared among all participants but the loss is borne by the leading provider, often a hospital. These programs have been mostly evaluated in the US where positive but non significant effects were observed: on volume, patient composition or quality of care. It seems that it could result in substantial savings, but only for trajectories with well-established care pathways (eg. joint replacements).

On this topic, it might be interesting to read one evaluation of such bundle-payment program, such as [Einav, Finkelstein, Ji, Mahoney,  Voluntary Regulation: Evidence from Medicare Payment Reform, 2022, The Quarterly Journal of Economics](https://academic.oup.com/qje/article/137/1/565/6372924).

- Shifting care to less costly settings: The goal is to move care from inpatient to outpatient settings where the costs are lower. The shift seems to be successful such a France, but might have increase volumes (see [Cazenave-Lacroutz, A., & Yilmaz, E. (2019). Dans quelle mesure les incitations tarifaires et la procédure de mise sous accord préalable ont-elles contribué au développement de la chirurgie ambulatoire?](https://ideas.repec.org/p/nse/doctra/g2019-05.html)).



## Conclusion

Overall, all 10 of these OECD countries are shifting away from pure DRG to a mixt including episode/population-based payments, global dotations with targets, add-on payment or quality incentives. Few of these programs have been evaluated. 