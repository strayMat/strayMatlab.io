---
blogpost: true
date: 2024-02-28
category: Papers
tags: [Statistics, Economics]
language: Français
---

# Methods to detect and measures the impact of a policy instrument on a time series

What are the methods used to detect the impact of a policy instrument on a time series? This post tracks my notes on the different existing methods : Interrupted Time Series (with ARIMA modelisation), Difference-In-Diffrence, synthetic controls, state-space models.


## Synthetic controls methods

[Bonander et al., 2021](https://pmc.ncbi.nlm.nih.gov/articles/PMC8634614/pdf/kwab211.pdf) give an excellent and detailled introduction to the SCMs with references to recent improvements. For health interventions, [Bouttell et al., 2018](https://jech.bmj.com/content/jech/72/8/673.full.pdf) give another introduction but which I found less clear.

[Abadie, A. (2021). Using synthetic controls: Feasibility, data requirements, and methodological aspects. Journal of economic literature, 59(2), 391-425.](https://pubs.aeaweb.org/doi/pdfplus/10.1257/jel.20191450) is the classic reference from Abadie who introduce the method in [Abadie et al., 2010](https://www.tandfonline.com/doi/abs/10.1198/jasa.2009.ap08746) for tabacco control.
 
### Theory

### Implementations

- Python packages: 

    - [pysyncon](https://github.com/sdfordham/pysyncon): more recent and active. It tried to reconcialiate the R packages synth and augsynth.
    - [SyntheticControlMethods](https://github.com/OscarEngelbrektson/SyntheticControlMethods)

- R packages: 

    - [augsynth](https://github.com/ebenmichael/augsynth): With three generalizations of the synthetic control, cross-validation and should be more stable than the original synth package. 
    - [synth](https://cran.r-project.org/web/packages/Synth/index.html): The original package.

## Causal Impact

The package is based on the paper [Brodersen et al., 2015](https://projecteuclid.org/journalArticle/Download?urlId=10.1214%2F14-AOAS788). The paper focuses on bayesian structural time series models. The model is fitted with a MCMC algorithm. The model is then used to estimate the counterfactual, that is, the expected outcome in the absence of the intervention. The impact is then estimated as the difference between the observed outcome and the counterfactual. The package is available in [R](https://github.com/google/CausalImpact) and [Python](https://github.com/jamalsenouci/causalimpact/). The idea is similar to synthetic controls but does not require control groups, predictor time-series (covariate) are used instead to model the time-serie of interest. 

## Interrupted time series (ITS)

This class of method draws from the literature on autocorrelated time series to models a single time-series before and after the intervention, then compare the parameters of the models to infer about the intervention impact. Many papers on this topic are written by James Lopez Bernal and his co-authors, such as the (good?) tutorial [Bernal et al., 2017](https://academic.oup.com/ije/article-pdf/46/1/348/24170929/dyw098.pdf). [Schaffer et al. 2021](https://bmcmedresmethodol.biomedcentral.com/counter/pdf/10.1186/s12874-021-01235-8.pdf) give a more recent tutorial with a detailed example in healthcare (nice graphs). 

By default, it does not make use of controls, thus being fragile to confounding factors near the intervention time. I am also concerned on possible overfitting of the model since the model form seems a bit hand-picked. However, this risk is mitigated by the small number of controls usually used in the model. These considerations on model selection are discussed in ([Bernal et al., 2018](https://researchonline.lshtm.ac.uk/id/eprint/4648066/1/A-Methodological-Framework-for-Model-Selection-in-Interrupted-Time-Series-Studies.pdf)).

### Minimal model for ITS 

As described by [Bernal et al., 2017](https://academic.oup.com/ije/article-pdf/46/1/348/24170929/dyw098.pdf), a minimal model for ITS is a simple regression : $Y_t = beta_0 + beta_1 t + beta_2 A_t + beta_3 t A_t$ where $Y_t$ is the outcome at time $t$, $A_t$ is the intervention indicator at time $t$, $beta_0$ is the baseline level of the outcome, $beta_1$ is the baseline trend, $beta_2$ is the immediate effect of the intervention and $beta_3$ is the change in trend after the intervention.

A strengh of these models is that they take into account the slowly varying confounders by modeling the long term trend (eg. socio-economic status). Rapidly changing confounders are not taken into account but can be added as supplementary regressors in the regression. 

Deseasonnalization should be done before fitting the regression since it often acts as a rapidly changing confounder. Auto-correlations could also be taken care of with ARIMA methods before fitting the regression if residuals exhibit auto-correlation.  

### ITS with confounders 

[Bernal et al., 2018](https://academic.oup.com/ije/article/47/6/2082/5049576) describe how to integrate  

### Causal analysis with ITS

Recent considerations on the causal aspect of ITS are discussed in [Mathes et al., 2024](https://pubmed.ncbi.nlm.nih.gov/39163255/), which detail crucial assumptions for ITS:  

- The trend after the intervention should have been the same as the trend before the intervention in the absence of the intervention
- A uncounfoundedness assumption, that is, pre-exposure/post-exposure groups should have similar caracteristics with respect to the outcome.

## Relationships between ITS and synthetic controls 

Some papers are discussing the links between the two methods : 

- [Linden, 2018](https://pubmed.ncbi.nlm.nih.gov/29356225/)

Discussions in the International Journal of Epidemiology are intersting to follow: (Benmarhnia eand Rudolph, 2019) critized ITS as being a weaker design than synthetic controls although being of the same family and questionned the lack of causal framework in ITS.This has been answered y (Bernal, et al. 2O19) with some arguments that I found a bit dishonest? For example, they argue that DID does not model time, which is true for simple DID but false for recent DID models that include time-varying confounder and time fixex effects (such as described in [Zeldow and Hatfield, 2021](https://onlinelibrary.wiley.com/doi/epdf/10.1111/1475-6773.13666)).

A recent comparison of both methods from [Nianogo et al., 2023](https://academic.oup.com/ije/article/52/5/1522/7110226) compare the performances of ITS and synthetic controls in a simulation study. The paper clearly state the differences and similarities between the different panel data methods with a modern causal vocabulary.  

For another point of view on the comparative advantages and pitfalls of the two approaches, see [Esposti et al., 2020](https://academic.oup.com/ije/article/49/6/2010/5917161?login=true). 

I understand the differences between ITS and synthetic controls as two closely related methods without strong causal framework. Both are fitting a pre-intervention models and look how different the post-intervention outcome is diffrent from this pre-intervention model. ITS seems to be more paremtric and hand-picked while synthetic controls are more data-driven and anchored in the statistal learning framework (train/test). Importantly, the estimand is estimated in ITS by the model coefficients (might be several coeffficienets depending of the form of the model: step, trend, lag...).   

## A recent application: Detect significant break in the emissions and evaluate the impact of  policy on the emission changes

Here, I focus on the [supplemental material of the paper](https://www.science.org/doi/suppl/10.1126/science.adl6547/suppl_file/science.adl6547_sm.pdf).

They use a linear model called saturated two-way-fixed-effects predicting $log(CO2)_{i,t}$ from treatment and time-period indicators, control variables (gdp, population, heating degree days, cooling degree days and eu) and fixed country and period effects. They use a variable selection method to detect break. This is not the lasso, the method is called GETS-panel and is based on a block-search algorithm. 


# References

- Abadie, A. (2021). Using synthetic controls: Feasibility, data requirements, and methodological aspects. Journal of economic literature, 59(2), 391-425.

- Mathes, T., Röding, D., Stegbauer, C., Laxy, M., & Pieper, D. Interrupted Time Series for Assessing the Causality of Intervention Effects. Part 35 of a Series on Evaluating Scientific Publications. Deutsches Arzteblatt international, (Forthcoming), arztebl-m2024.

- Bernal, J. L., Cummins, S., & Gasparrini, A. (2017). Interrupted time series regression for the evaluation of public health interventions: a tutorial. International journal of epidemiology, 46(1), 348-355.

- Linden, A. (2018). Combining synthetic controls and interrupted time series analysis to improve causal inference in program evaluation. Journal of evaluation in clinical practice, 24(2), 447-453.

- Bernal, J. L., Cummins, S., & Gasparrini, A. (2018). The use of controls in interrupted time series studies of public health interventions. International journal of epidemiology, 47(6), 2082-2093.

- Bernal, J. L., Soumerai, S., & Gasparrini, A. (2018). A methodological framework for model selection in interrupted time series studies. Journal of clinical epidemiology, 103, 82-91.

- Benmarhnia, T., & Rudolph, K. E. (2019). A rose by any other name still needs to be identified (with plausible assumptions). International journal of epidemiology, 48(6), 2061-2062.

- Bernal, J. L., Cummins, S., & Gasparrini, A. (2019). Difference in difference, controlled interrupted time series and synthetic controls. International journal of epidemiology, 48(6), 2062-2063.

- Schaffer, A. L., Dobbins, T. A., & Pearson, S. A. (2021). Interrupted time series analysis using autoregressive integrated moving average (ARIMA) models: a guide for evaluating large-scale health interventions. BMC medical research methodology, 21, 1-12.

- Zhang, Y., & Rottman, B. M. (2023). Causal learning with interrupted time series data. Judgment and Decision Making, 18, e30.

- Bouttell, J., Craig, P., Lewsey, J., Robinson, M., & Popham, F. (2018). Synthetic control methodology as a tool for evaluating population-level health interventions. J Epidemiol Community Health, 72(8), 673-678.

- Zeldow, B., & Hatfield, L. A. (2021). Confounding and regression adjustment in difference‐in‐differences studies. Health services research, 56(5), 932-941.

- Nianogo, R. A., Benmarhnia, T., & O’Neill, S. (2023). A comparison of quasi-experimental methods with data before and after an intervention: an introduction for epidemiologists and a simulation study. International Journal of Epidemiology, 52(5), 1522-1533.

- Degli Esposti, M., Spreckelsen, T., Gasparrini, A., Wiebe, D. J., Bonander, C., Yakubovich, A. R., & Humphreys, D. K. (2020). Can synthetic controls improve causal inference in interrupted time series evaluations of public health interventions?. International journal of epidemiology, 49(6), 2010-2020.