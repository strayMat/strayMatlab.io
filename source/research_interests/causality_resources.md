# Causality resources

## Time-varying designs

- [The R taskview on causal inference
  implementations](https://cran.r-project.org/web/views/CausalInference.html)

- [lmtp](https://github.com/nt-williams/lmtp)

## Propensity matching

- Matching methods for causal inference, Sizemore and Alkurdi, 2019: A review
  that seems well written on matchingg
