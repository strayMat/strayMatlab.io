# Projet de recherche 

## Thématiques et questions

> Selon la définition du bureau européen de l’Organisation mondiale de la santé (OMS) en 1994 lors de la conférence d’Helsinki, « la santé environnementale comprend les aspects de la santé humaine, y compris la qualité de la vie, qui sont déterminés par les facteurs physiques, chimiques, biologiques, sociaux, psychosociaux et esthétiques de notre environnement. Elle concerne également la politique et les pratiques de gestion, de résorption, de contrôle et de prévention des facteurs environnementaux susceptibles d’affecter la santé des générations actuelles et futures 

Les thématiques santé-environnement les plus souvent abordées sont : la pollution, l'alimentation, la prévention, l'exposition au changement climatique, l'exposition sociale [CITATION needed]. 

Au sein de cette large thématique, plusieurs questions générales se posent : Quels systèmes de santé sont les plus résilients au changement climatique ? Quelle offre de soin "minimale" est nécessaire étant données les contraintes économiques pesant sur le secteur de la santé dans un contexte démographique défavorable ? Quelle caractéristique de cette offre de soin (ambulatoire et hospitalières, place des compétences para-médicale et de la prévention) ? Quelle est l'association entre la littératie en santé et la santé ?  

- Préventions et actions pour la réductions des maladies chroniques: Le PEPR SAMS [Inrae-Inserm](https://www.enseignementsup-recherche.gouv.fr/fr/le-pepr-sams-pour-une-alimentation-saine-et-durable-94704) se penche sur ces sujets concernant l'alimentation: [cf. Présentation du PEPR SAMS](https://anr.fr/fileadmin/aap/2023/France2030-PEPR-SAMS-AAP-2023-22112023.pdf). Aborder cet axe par le côté effets individuels/individualisés et effets de sous groupe. La prévention et la pertinence des soins sont des axes forts de la santé environnement [cf. feuille de route
  HAS](https://www.has-sante.fr/jcms/p_3475967/fr/la-has-adopte-une-feuille-de-route-sante-environnement). Il serait super intéressant de croiser sur des croisements de données de santé et de consommation (ex. données de caisse) pour voir l'effet de l'étiquetage alimentaire sur les pathologies avec des facteurs de risques liées à l'alimentation (diabète, obésité, cardio-vasculaire). Un croisement individuelle est impossible mais je serai déjà curieux de regarder les interactions à un niveau communal. Ensuite, il faudrait réfléchir à une stratégie d'identification : pê une approche instrumentale avec des variations de prix. Cependant, les outcomes sont longs terme donc ce n'est pas évident.   

- Question des modèles de risque à l'échelle fine : si on connaît bien le risque, on doit être capable de lui attribuer un coût et également dans certains cas, un coût evitable (ex. inondation/chaleur). En santé, cette question du risque est liée à l'AM (assureur/financeur) et aux mutuelles qui sont les principaux intéressés pour des modèles de risque individuel (ou global sur leur population, mais il faut a minima que le modèle soit bien calibré pour avoir une estimé correcte de l'utilité, cf. [Perez et al., 2023](https://arxiv.org/pdf/2210.16315)) pour aller vers du mieux soigner et de la prévention à toutes les étapes.

- Quels effets de la sédentarité ? Possibilité de trouver des données sur le nombre de véhicules, sur le nombre de pistes cyclables (suffisamment fin pour faire de l'ajustement ?), score de marchabilité? 

- Quel apport des EDS : source ET moyen moderne de collecte de données: par exemple, calcul de l'espérance de vie en bonne santé (par approximation du [GALI](https://drees.solidarites-sante.gouv.fr/sites/default/files/2023-02/ER1258EMB.pdf), obtenu pr l'instant grâce à l'enquête SRCV). Biais du recours au soin cependant et il faudrait des self-questionnaires pour le gali donc assez foireux. Mesure du poids ? du tabagisme ?  La mise en place de l'EDS de ville permettrait de faire ce genre de mesure à moindre coût. 

### Pollution industrielle et santé

- projet BIS (Bassins Industriels et Santé) porté par Santé publique France (SpF), en partenariat avec l’Institut national de l’environnement industriel et des risques (INERIS), vise à déterminer l’association entre la proximité de grands bassins industriels et l’état de santé de la population riveraine en construisant des indicateurs d’exposition et de pressions environnementales liés aux émissions industrielles, puis en les croisant avec des indicateurs sanitaires. Pas d'information sur ce projet depuis une com HDH de 2023. SPF a bossé sur un [recensement des bassins industriels en 2020](https://www.santepubliquefrance.fr/docs/pertinence-d-une-surveillance-epidemiologique-autour-des-grands-bassins-industriels.-etape-1-recensement-des-bassins-industriels-et-bilan-des-etu). 

- SPF a travaillé sur la surmortalité en lien avec [la pollution de l'air en 2019](https://europepmc.org/article/MED/26298834). Ils ont également travaillé sur l'impact de la [baisse de pollution dûe au confinement](https://www.amse-aixmarseille.fr/fr/dialogeco/pollution-de-l%E2%80%99air-ambiant-et-mortalit%C3%A9-en-france-nouvelles-estimations-de-l%E2%80%99impact) et sur [l'évaluation économique de l'impact de la pollution aux particules fines sur la mortalité en 2017](https://www.amse-aixmarseille.fr/sites/default/files/actu/synthese_evaluation_economique_eqis_france_2016.pdf). 

- Fardeau sanitaire (_burden of disease_) et coût de la pollution de l'air : L'étude de 2022 de l'AEE estime le coût de la pollution de l'air des plus gros sites industriels à 15.5 milliards d'euros par an en France. La mortalité est estimé pour les principaux polluants ($O_3$, $PM_{2.5}$, $NO_2$) en se basant sur des [courbes dose-réponses recommandées par l'OMS](https://apps.who.int/iris/handle/10665/345329). La morbidité est estimée en utilisant des données d'association entre 10 paires polluant/maladie. 

- Estimation du coût de la pollution : En France, ces chiffres ont été estimés par un rapport [du sénat en 2015](https://www.senat.fr/travaux-parlementaires/structures-temporaires/commissions-denquete/commissions-denquete/commission-denquete-sur-le-cout-economique-et-financier-de-la-pollution-de-lair.html) à 3 M par an directement imputable à la prise en charge des maladies liés à la pollution de l'air (pas d'étude citée), et 68 à 97 M de coûts indirectes (mortalité et morbidités imputables calculés par l'étude CAFE, Air pur pour l’Europe) et remis en perspective dans [un rapport de 2022 relatif à l'adaptation de la sécurité sociale à ](https://www.senat.fr/rap/r21-594/r21-5947.html).

- Un apport intéressant pourrait être l'analyse des lieux de décès (dont dispose l'INSEE, cf. DDAR  avec Sébastien Durier ou DSAU avec Laurent Lequien. Une de ces deux divisions géolocalise les certificats de décès). On n'aurait pas de données de santé mais on pourrait estimer finement les associations entre pollution et décès. Le tuyau vient d'Elise Coudin. Il faut les contacter. Ca ferait aussi un super sujet sur la partie choc des îlots de chaleur. 

- Apport EDP : La quasi-totalité de ces études se focalisent sur la mortalité. Il pourrait être intéressant de regarder les associations de la pollution sur des indicateurs de santé plus fins (ex. morbidité, activité).

- Apprentissage et pollution de l'air ? On pourrait regarder le même genre d'instrument que celui des études de Milena si on appariait aux données de l'éducation nationale : Bénédicte a bossé sur une base qui contenait des informations sur les résultats des enfants de toute une classe d'âge.

### Croisement santé / environnement / travail 

- Quelle possibilité de croiser les données de bien-être ressenti ([enquête SRCV](https://data.progedo.fr/studies/doi/10.13144/lil-1646), Statistiques sur les Ressources et les Conditions
de Vie) et les données de santé du SNDS ? Cela pourrait être très intéressant pour explorer plus finement les interactions revenu/santé/satisfaction comme évoqué dans la [publication de Jean-Marc Germain en juin 2024](https://www.insee.fr/fr/statistiques/fichier/version-html/8199183/DT2024-09.pdf).

- [Estimation du fardeau environnemental et professionnel de la maladie](https://www.santepubliquefrance.fr/les-actualites/2024/estimation-du-fardeau-environnemental-et-professionnel-de-la-maladie-en-france-quels-sont-les-couples-maladie-facteur-de-risque-a-prioriser) : SPF va prioriser l'estimation du fardeau AVC-PM2.5. 

- Qualité des soins et efficiences des ressources : Evaluation d'Ifaq avec le choc de financement de 2022. Sur la question de l'effet de la mise en concurrence qualité, un article de référence au Pays-Bas est cité dans le rapport IGAS 2023 sur le financement qualité. TODO: existe-t-il des articles semblables pour la France ? [Varkevisser et al., 2012](https://www.sciencedirect.com/science/article/pii/S0167629612000082?via%3Dihub) Ils font ça dans le cas de l'angioplastie.  En France, c'est Anne Girault de l'EHESP qui semble un acteur essentiel de cette question du financement à la qualité avec notamment cette étude mixte questionnaire-entretiens : [Girault, et al. 2019](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7008690/). 

### Impact du nombre de soignants sur la qualité  des soins

- une loi a été voté en 2025 pour établir un nombre minimum de soignants paramédicaux pour chaque patient. c'est une fourchette de ratio qui est prévu. La HAS a deux ans pour les définir pour chaque spécialité. La Californie, le Nevada ou dans le Queensland en Australie ont déjà fait ça avec apparemment des résultats positif sur les réhospitalisations et la mortalité.

Explorer l'hte actuelle serait intéressant ainsi que le suivi de la mise en place de ces ratios : la DSN publique pourrait répondre à ces questions il me semble. Contact Lino Galiena pour monter le projet. Sujet potentiel avec Pierre je pense car géographique et dans la lignée de ses fils d'attentes. 

### Accès aux soins: besoins et demandes

- Sujet Irdes, [Typologie communale aux soins de premiers recours](https://ij-healthgeographics.biomedcentral.com/articles/10.1186/s12942-024-00366-7): Marie Bonal, Véronique Lucas Gabrielli. Le projet se fait dans un cadre européen appelé [Oases](https://oasesproject.eu/). Ils incluent principalement les APL aux MGs, les les scores de prof de santé de proximité, les services de proximité intermédiaires, la istance aux urgences, les évolutions de l'offre au MG, le revenu, les
  scores de besoin potentiels de la population.  Ils font deux ACP de suite en incluant seulement sur la deuxième les deux dimensions qui leurs paraissent très importantes (APL aux MGs et distance aux urgences). Puis font une classification hiérarchique pour trouver 7 classes de communes. 

## Méthodologies 

- Evaluation pour les politiques publiques: quel rôle du ML ? 

- Cost-benefice nécessaire afin de montrer les externalités du changement climatique sur la santé (et de quantifier des impacts longs termes)

- Comment estimer des effets sur des outcome long termes et/ou composites (comme la multi-morbidité)

- Référentiel des moyens d'actions sur chaque thématique (gros projet): Avant d'évaluer quelles sont les actions ? 

## Gros appariements

- Appariement base des données de caisse avec les données de santé (EDP-santé/SNDS/EDS): Iris, année. Question: quelle effet de l'étiquetage alimentaire sur les pathologies avec des facteurs de risques liées à l'alimentation (diabète, obésité, cardio-vasculaire) ? Pb: temporalité longue et appariement non individuel. On aurait des choses intéressantes si on regarde des indicateurs biologiques de type IMC ou glyclipédie chez des patients particuliers (ex. diabétique), mais cela nécessite d'aller sur de l'individuel. 

- Rénovation énergétique et état de santé ? Enjeux chaleur/qualité du logement. Si on arrive à apparier SNDS et les factures d'énergie, on pourrait étudier l'effet de l'isolation en estimant à quel point le logement est une passoire l'hiver. Idéalement on pourrait monter une expérience randomisée sur le sujet en proposant de finançer de la rénovation (on en distribue beaucoup, ça pourrait être intéressant d'avoir une part de randomisé). 

## Références

- [Evaluation globale des Plans nationaux santé, HCSP, 2022](https://www.hcsp.fr/explore.cgi/avisrapportsdomaine?clefr=1223)
- [Plannification écologique du système de santé](https://sante.gouv.fr/IMG/pdf/planification-ecologique-du-systeme-de-sante-feuille-de-route-mai-2023.pdf)
- [Feuille de route Santé Environnement de la HAS](https://www.has-sante.fr/upload/docs/application/pdf/2023-11/feuille_de_route_sante_environnement_has.pdf) : Bon usage, prévention (secondaire, tertiaire), réduire la sur-prescription, ou prescription inadaptée (médicaments, examens, DMs), [fiches pertinences](https://www.has-sante.fr/jcms/r_1499655/fr/pertinence-des-soins)
- [Propositions de la CNAM pour 2024](https://assurance-maladie.ameli.fr/sites/default/files/2023-07_synthese-rapport-propositions-pour-2024_assurance-maladie.pdf)
- [Registre de traitement Drees-OSAM, 2023](https://sante.gouv.fr/IMG/pdf/registre_drees-osam.pdf)
- [Registre de traitement DGS environnement 2023](https://sante.gouv.fr/IMG/pdf/registre_dgs_ea.pdf)
- [Répertoire de traitement HDH remis en forme par PA](https://grist.incubateur.net/o/has/3yWxTziAxQ1U/Repertoire-projets-SNDS-du-HDH)
- [Cours environnement et santé publique: Goupil-Sormany, I., Debia, M., Glorennec, P., Gonzalez, J. P., Noisel, N., Dab, W., ... & Daubas-Letourneux, V. (2023). Environnement et santé publique.](https://www.cairn.info/environnement-et-sante-publique--9782810910076.htm)
- [Atlas européen de la santé environnementale, 2023](https://discomap.eea.europa.eu/atlas/)
- [Programme de travail SPF, 2024](https://www.santepubliquefrance.fr/a-propos/notre-organisation/nos-objectifs-notre-programme-de-travail/programme-de-travail-2024.-sante-publique-france)

## 2024-07-03 - Echange avec Milena Suarez

L'EDP est sous-utilisé, ils portent le projet agriculture avec Inès au D2E, mais rien d'autre pour l'instant. Ils sont en interaction avec Béatrice Geoffroy Perez de SPF, qui a beaucoup travaillé sur les liens santé-travail.  

Trois sujets émergent comme potentiellement intéressant dont deux avec l'EDP et un avec le CepiDc :

- Travail : Côté Drees, ils apparient l'EDP avec les causes de décès exhaustives, en faisant des indicateurs d'espérances de vie. On a les professions quand la personne est recensé et salarié. Assez exhaustif pour les salariés sinon il faut restreindre au recensement de 1999 avec 1% de la population. Croisement avec les expositions. [Sur l'amiante, SPF a abandonné la surveillance sanitaire](https://www.santepubliquefrance.fr/maladies-et-traumatismes/cancers/mesotheliomes/documents/rapport-synthese/programme-national-de-surveillance-du-mesotheliome-pleural-pnsm-vingt-annees-de-surveillance-1998-2017-des-cas-de-mesotheliome-de-leurs-expo). Il y a des travaux sur les arrêts de travail avec la base IJ (???) en croisement avec la pollution de l'air. [Marion Leroutier](https://marionleroutier.github.io/research/). Cette dernière a peut-être un projet avec [Julia Mink](https://www.juliamink.eu/). Mais pas grand chose avec les autres professions. Cela pourrait être intéressant de faire des gradients profession, revenus, localisation, recours aux soins, IJ. Pour cela, il faudrait bosser avec[Béatrice Geofroy Perez](https://www.santepubliquefrance.fr/docs/programme-cosmop-surveillance-de-la-mortalite-par-cause-selon-l-activite-professionnelle-analyse-de-la-mortalite-et-des-causes-de-deces-par-sec) fait partie du groupe utilisatrice de l'EDP. 

  Il y aurait des interactions pertinentes avec le projet BIS en tenant compte de la profession et du revenu : le désavantage est qu'on se restreindrait aux 4% des salariés ouvriers. La difficulté est de trouver des contrôles identiques sur le plan niveau de vie et pathologies, par exemple en prenant des contrôles négatifs non liées à l'industrie (ex. diabète? Pas sûr car impacts hormonaux forts).

- Effets des îlots de chaleur sur la mortalité au niveau fin : géolocalisation des décès et exposition aux vagues de chaleur : ça pourrait être super stylé selon Milena. Il faut vérifier que les données du CepiDc sont géolocalisées et que personne n'est déjà en train de faire ça.

- Qualité de l'eau : c'est au niveau communal, les données sur l'eau devraient être ouverte au public. Le principal problème est de savoir s'il y a des impacts courts ou moyens termes sur la santé. Un point d'attention, c'est fait-on cela via le SNDS ou via l'EDP qui a moins de puissance statistique.  


## Contacts 

- Erik-André Sauleau, données géographiques et bayésien CHU Strasbourg
