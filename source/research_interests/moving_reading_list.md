# 📜 Reading list

I spent a non-negligible part of my time opening new browser tabs with blog posts and papers luring to understand new concepts in machine learning, statistics, public health and social science. I never take the time to read them, but I keep adding them here. It increases the chance that I read them at some point.   

## Statistics 

### Causal Inference

- [Modern causal inference, Alejandro Schuler, 2024-WIP](https://lorenzof.quarto.pub/introduction-to-modern-causal-inference/Introduction-to-Modern-Causal-Inference.pdf) : Influence function stake. The pdf book seems in progress, the [online version](https://alejandroschuler.github.io/mci) has more content in june 2024.

- [Chernozhukov book on causal ML](https://causalml-book.org/)

- ✅ [Wager course on causal inference, Stats-361](https://web.stanford.edu/~swager/stats361.pdf) : Super clear and quite accessible book on causal inference by Wager with a focus on econometrics.
 
- [Measurement bias and effect restoration in causal inference](https://academic.oup.com/biomet/article-abstract/101/2/423/194920?redirectedFrom=fulltext), Kuroki and Pearl, 2014 foundations on proxy learning. 

- [The Hades toolbox for observational studies](https://ohdsi.github.io/Hades/packages.html): All packages developped and maintained by Ohdsi

- [Off-policy evaluation](https://sites.google.com/cornell.edu/recsys2021tutorial) 

- ✅ [The E-value, measuring the effect of potential unmeasured counfounder](https://www.acpjournals.org/doi/epdf/10.7326/M16-2607) : A simple tool to measure what should be the effect of a confounder to shift the estimate.

### Large Language Models

- [Training Large Language Models to Reason in a Continuous Latent Space](https://arxiv.org/abs/2412.06769): Meta paper on reasonning tokens in LLMs, recommended by Simmon Willison.

### Machine Learning

- [factorization machine](https://www.jefkine.com/recsys/2017/03/27/factorization-machines/) : A mix between matrix factorization and SVM for recommander systems.
- [Implicit Layer](http://implicit-layers-tutorial.org/) : Neural ODE stuffs, continuous process.
- ✅ [Language Models: A guide for the Perplexed, 2023](https://arxiv.org/pdf/2311.17301.pdf): un cours de Noah Smith de AI2.
- [Course on LLM](https://www.youtube.com/watch?v=Ku9PM26Cc2c) 

### Other statistical stuffs

- Geographical statistics: [Introduction à la gémoatique poour le statisticien, Semecurbe, Coudin, 2022](https://www.researchgate.net/profile/Elise-Coudin/publication/358535902_Introduction_a_la_geomatique_pour_le_statisticien_quelques_concepts_et_outils_innovants_de_gestion_traitement_et_diffusion_de_l'information_spatiale_Documents_de_travail/links/62069ac787866404a16052b6/Introduction-a-la-geomatique-pour-le-statisticien-quelques-concepts-et-outils-innovants-de-gestion-traitement-et-diffusion-de-linformation-spatiale-Documents-de-travail.pdf)


### Healthcare 

- [Training on grade](https://training.cochrane.org/introduction-grade)

#### Sustainable healthcare

- [Review on healthcare system sustainability, Seppänen et Or, Irdes, 2023](https://www.irdes.fr/english/reports/586-the-environmental-sustainability-of-health-care-systems.pdf)

- [Sector synthesis from the Shift Project, 2023](https://theshiftproject.org/wp-content/uploads/2023/04/180423-TSP-PTEF-Synthese-Sante_v2.pdf) 

- [How sustainable is High tech Healthcare, Kris De Decker, 2022](https://solar.lowtechmagazine.com/2021/02/how-sustainable-is-high-tech-health-care.html) 

#### Healthcare economy

- [France Healthcare system review 2023, Irdes](https://eurohealthobservatory.who.int/publications/i/france-health-system-review-2023)

- [Enquêtes sur les bénéficiaires de l'expérimentation PEPS, 2024, Irdes](https://www.irdes.fr/recherche/questions-d-economie-de-la-sante/285-mise-en-oeuvre-effets-et-usages-de-l-experimentation-peps-dans-16-centres-de-sante.pdf)

- ✅ [Mousquès, J. (2023). Déserts médicaux en soins de premier recours: un regard économique. Les Tribunes de la santé, (4), 57-63.](https://www.cairn.info/revue-les-tribunes-de-la-sante-2023-4-page-57.htm?ref=doi): Point d'entrée intéressant sur la relation offre demande en santé en France. 

- [Basu A, Carlson J, Veenstra D. Health years in total: a new health objective function for cost-effectiveness analysis. Value Health. 2020](http://www.sciencedirect.com/science/article/pii/S1098301519351873?via%3Dihub)


### Sustainable technology

- [Murray Bookchin, vers une technologie libératrice](Vers une technologie libératrice)

### Economy 

- [Cost management and behaviors, by Riveline](http://riveline.net/#vid)

### Sociology

- Hartmut Rosa, Accélération. Une critique sociale du temps
- Harry M. Marks, The Progress of Experiment: Science and Therapeutic Reform in the United States
