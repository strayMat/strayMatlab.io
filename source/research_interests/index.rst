👨‍🔬 Research projects
***********************

.. toctree::
  :maxdepth: 1
  :hidden:

  snds2vec
  moving_reading_list

Interests
---------

- Public Health
- Causal inference 
- Sociology
- Representation learning
- NLP for information extraction

At the intersection of these fields I am asking questions such as : 

- How to build robust policy evaluation trough opportunistic data collection ? (real world)

- What are the influence of data transformation on treatment effect inference ? 


Research projects 
-----------------


.. card:: **Causal thinking for decision making on Electronic Health Records: why and how**

    .. image:: /_static/files/images/why_and_how.png
      :width: 100%
      :alt: Causal thinking
    
    - Preprint: `Doutreligne, M., Struja, T., Abecassis, J., Morgand, C., Celi, L. A., & Varoquaux, G. (2023). Causal thinking for decision making on Electronic Health Records: why and how. <https://hal.science/hal-04174834v2/document>`_
    -  `Oral presentation at Clalit's innovation team, 2023. <../_static/files/presentations/2023-08-15_clalit_causal_thinking_with_ehr.pdf>`_
 

.. card:: **Review of Hospital Data Warehouse in France**

    .. image:: /_static/files/images/edsh_rapport.png
      :width: 100%
      :alt: Hospital Data Warehouse
    
    - Publication: `Doutreligne M, Degremont A, Jachiet PA, Lamer A, Tannier X (2023) Good practices for clinical data warehouse implementation: A case study in France. PLOS Digital Health <https://journals.plos.org/digitalhealth/article?id=10.1371/journal.pdig.0000298>`_
    - Oral presentation at the French Epidemiogy congress, Emois: `Doutreligne, M., Degremont, A., Jachiet, P. A., Lamer, A., & Tannier, X. (2023). Panorama des entrepôts de données hospitaliers dans les CHU/CHR de France. Revue d'Épidémiologie et de Santé Publique, 71, 101463. <../_static/files/presentations/2023-03-17_D1-1-DOUTRELIGNE.pdf>`_
    - `Extended report for the French High Authority of Health <https://has-sante.fr/jcms/p_3386124/fr/entrepots-de-donnees-de-sante-hospitaliers-en-france-rapport-octobre-2022>`_
 

.. card:: **How to select predictive models for causal inference?**

    .. image:: /_static/files/images/causal_model_selection.png
      :width: 100%
      :alt: Causal model Selection 

    - Preprint: `Doutreligne, M. & Varoquaux G. (2023). How to select predictive models for causal inference?. arXiv preprint arXiv:2302.00370. <https://arxiv.org/abs/2302.00370>`_
    - Poster at the International Conference of Statistic and Data Science, 2022: `Causal model selection: is my metric adapted ? <../_static/files/presentations/Causal_model_selection___is_my_metric_adapted_____poster_ICSDS_2022.pdf>`_

.. card:: **Transfer Event representations between claims and EHR for diagnosis and prognosis**
    
    .. image:: /_static/files/images/snds2vec_annotated_short.png
      :width: 100%
      :alt: Treatment effect motivation 

    - Oral presentation: `ATALA seminar on patient embeddings, 2023-03-13 <../_static/files/presentations/2023-03-30_event2vec-labsante-drees.pdf>`_
    
    - Project status: ongoing, experimental evaluation


Treatment effect estimation from EHR data 
_________________________________________

    .. image:: /_static/files/images/medical_applications.png
      :width: 100%
      :alt: Treatment effect framework 

.. card:: **Reevaluation of second line oral diabetics on HBA3C control and weight loss**

    - Title: Observational case control study on long term effect of GLP1-RA
    
    - Abstract: The primary objective of the study is to assess the short term
      effect of GLP1-RA on weight loss. The effectiveness on the HBA1C diminution will be assessed as a control outcome. Despite clinical studies that proved the efficacy of GLP1-RA on weight loss, there is currently a lack of observational studies on the long term effect of GLP1-RA on weight loss.

    The secondary objective is to study the long term effects on morbidity, rehospitalization and mortality.

    Finally, the third objective is the analysis of the heterogeneity of the
    effect of GLP1-RA on different subpopulations. The patient stratification will be made on the following baseline factors: age, sex, BMI, type 2 diabetes history length, cardio-vascular background, tritherapy status.  Project status: ongoing, extraction  and description of the target population

.. card:: **Chronic Kidney Disease: Effect of the timing of initialization of dialysis on mortality**

    - Project presentation: `Greater Paris hospital, datascientist meetup, 2023-03-09 <https://soda.gitlabpages.inria.fr/deepacau/>`_
    - Project status: *Paused*

.. card:: **Evaluation of medical imaging on mortality for patients hospitalized in intense care unit**

    - Project presentation: `Inria Soda internal seminar, 2021-11-16 <../_static/files/presentations/2021-11-16_soda_presentation_mimic_work.pdf>`_
    - Project status: *Paused.*


