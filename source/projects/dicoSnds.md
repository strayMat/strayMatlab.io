---
readingShow: true 
---

# [Dico snds](https://health-data-hub.shinyapps.io/dico-snds/)

The [SNDS](https://documentation-snds.health-data-hub.fr/) (for Système National des Données de Santé) is the centralized healthcare 
database based on the French National Health Insurance ([CNAM](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjKkqSdhtXpAhUpzIUKHTGmAcAQFjAAegQIAxAB&url=https%3A%2F%2Fassurance-maladie.ameli.fr%2Fqui-sommes-nous%2Forganisation%2Fcnam-tete-de-reseau%2Fcnam-tete-reseau&usg=AOvVaw37SZRctRucqOhZhoRtXRJ-)). 
With almost 5 billions of healthcare services per year and 66 millions of people, 
it is a unique source of information for healthcare research and public health monitoring.

I dived into this monstruous database in september 2018. 
Thanks to the information of the CNAM and the help of co-workers at the departement 
of health statistics (DREES), I built an interactive dictionnary to navigate into 
the structure of this huge and complex database. 

This tool is called [dico-snds](https://health-data-hub.shinyapps.io/dico-snds/), it is [opensource](https://gitlab.com/healthdatahub/dico-snds) and is 
now used by a large part of the community working on the SNDS.
