---
readingShow: true 
---

# [NeuroNer](https://github.com/strayMat/tag_serve)

A [Wind-EDS](https://github.com/EDS-APHP) project in the team of Nicolas Paris at the Paris hospitals.

Paris hospitals have millions of medical reports that could not be used in intern research projects because of privacy issues. 
These texts contain numerous personal information that cannot be shared such as addresses, phone numbers, names ...
The heterogeneity of this information makes it delicate to automatically pseudonymised these reports.

We built a mixed model with a rule based tool and a neural component in order to get the best of both worlds. This tool is currently in production at the APHP.

We are currently trying to package this tool [here](https://framagit.org/interchu/omop-note-deid) for other hospitals to use it.


