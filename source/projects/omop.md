# OMOP

> Helping normalize medical databases formats

The progressive consolidation of medico-administrative databases requires more interoperability and 
standardization. OMOP is a common model used internationally by multiple academic and industrial 
actors for the analysis of health data ("Electronic Health Records"). By proposing a common data 
structure and semantics, it allows instant portability of analysis tools, facilitating the realization
of studies in different structures.
 
I work with various entities to help map medical data and more precisely the French National Health 
Data System, [SNDS](https://documentation-snds.health-data-hub.fr/) to this format in the framework of interChu, a French network of OMOP databases.

It is an ongoing project that mobilizes a large part of the French medical informatics ecosystem.

We presented [a poster](../_static/files/omop_printable_poster.pdf) describing a mapping software from the SNDS default schema to the OMOP CDM 
at the [EMOIS](https://emois.org/) congress : Doutreligne et al, 2020 [^1].

[^1]: [M. Doutreligne, D.-P. Nguyen, A. Parot, A. Lamer, et N. Paris, « Alignement à grande échelle du Système des données de santé vers le modèle commun de données OMOP », Revue d’Épidémiologie et de Santé Publique, vol. 68, p. S37, mars 2020, doi: 10.1016/j.respe.2020.01.081](https://www.sciencedirect.com/science/article/abs/pii/S0398762020300894)
